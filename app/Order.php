<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order extends Model
{
    use Userstamps;
    use LogsActivity;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotel_id', 'room_id', 'customer_id', 'payment_status','checkedin_status','book_start','book_end','checkedout_at'
    ];

    protected static $logFillable = true;

    public function room()
    {
        return $this->belongsTo('App\Room');
    }
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
    public function user()
    {
        return $this->belongsTo('App\User','customer_id','id');
    }
}

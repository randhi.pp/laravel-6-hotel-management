<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;


class Room extends Model
{
    use Userstamps;
    use LogsActivity;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotel_id', 'number', 'type', 'price'
    ];

    protected static $logFillable = true;

    // public function hotel()
    // {
    //     return $this->belongsTo('App\Hotel');
    // }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Hotel;
use App\Room;
use App\Order;
use App\Staff;
use App\User;
use Auth;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRoom(Request $request)
    {
        if ( !$request->ajax() )
            return abort('404','unauthorized');

        $room = Room::where('hotel_id', $request->hotel_id)
                    ->where('type',$request->room)
                    ->get();

        return $room;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function apiIndex(Request $request)
    {
        $user = auth('api')->user();


        $rooms = Room::where('hotel_id',$request->id);

        if ( $request->ajax() ) {
            $rooms = Room::select(DB::raw("count(case when type = 'standar' then 1 end) as tstandar,
            count(case when type = 'double' then 1 end) as tdouble,
            count(case when type = 'studio' then 1 end) as tstudio,
            count(case when type = 'executive' then 1 end) as texecutive,
            count(case when type = 'deluxe' then 1 end) as tdeluxe,
            count(case when type = 'suite' then 1 end) as tsuite"))
            ->where('hotel_id', $request->id )
            ->get();

            dd($rooms);

            return $rooms;

        }
        else {
            if ( $user->hasRole('admin') )
                $rooms = $rooms->get();

            if ( $user->hasRole('manager') ) {
                $managerAt = Hotel::where('manager_id',$user->id)->get('id')->toArray();
                if ( !in_array( $request->id , $managerAt ))
                    $rooms = $rooms->get();
            }
            if ( $user->hasRole('staff') ) {
                $staffAt = Staff::where('user_id',$user->id)->get();
                if ( $request->id !== $staffAt->hotel_id )
                    $rooms = $rooms->get();
            }
        }


        return response()->json([
            'data' => $rooms
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function availableRoom(Request $request)
    {
        $user = auth('api')->user();

        // exclude booked room
        // select room_id from orders where hotel_id = 2

        $booked = Order::where('hotel_id',$request->id)
                        ->whereDate('book_start', '<=' , $request->date)
                        ->whereDate('book_end', '>=' , $request->date)
                        ->get();

        //    dd($booked);

        $rooms = Room::where('hotel_id',$request->id);
        if ( $request->ajax() )
            $rooms = $rooms->get();
        if ( $user->hasRole('admin') )
            $rooms = $rooms->get();

        if ( $user->hasRole('manager') ) {
            $managerAt = Hotel::where('manager_id',$user->id)->get('id')->toArray();
            if ( !in_array( $request->id , $managerAt ))
                $rooms = $rooms->get();
        }
        if ( $user->hasRole('staff') ) {
            $staffAt = Staff::where('user_id',$user->id)->get();
            if ( $request->id !== $staffAt->hotel_id )
                $rooms = $rooms->get();
        }

        return response()->json([
            'data' => $rooms
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bookedSearch(Request $request)
    {
        $user = auth('api')->user();

        // exclude booked room
        // select room_id from orders where hotel_id = 2

        $booked = Order::with('customer','hotel','room')->where('hotel_id',$request->id)
                        ->whereDate('book_start', '<=' , $request->date)
                        ->whereDate('book_end', '>=' , $request->date)
                        ->get();

            // dd($booked);

        // $rooms = Room::where('hotel_id',$request->id)
        //             ;

        // if ( $user->hasRole('admin') )
        //     $rooms = $rooms->get();

        // if ( $user->hasRole('manager') ) {
        //     $managerAt = Hotel::where('manager_id',$user->id)->get('id')->toArray();
        //     if ( !in_array( $request->id , $managerAt ))
        //         $rooms = $rooms->get();
        // }
        // if ( $user->hasRole('staff') ) {
        //     $staffAt = Staff::where('user_id',$user->id)->get();
        //     if ( $request->id !== $staffAt->hotel_id )
        //         $rooms = $rooms->get();
        // }

        return response()->json([
            'data' => $booked
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Hotel;
use App\Staff;
use App\User;

use Carbon\Carbon;
use Auth;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        if(Auth::user()->hasRole('admin'))
            $staffs = Staff::paginate(15);

        
        if(Auth::user()->hasRole('manager')) {
            $hotels = Hotel::where('manager_id', Auth::user()->id )->get()->toArray();

            foreach ($hotels as $key => $value) {
                $hotelArray[] = $value['id'];
            }

            $staffs = Staff::whereIn('hotel_id', $hotelArray )->paginate(15);
        }
            
        return view('internal.staff.index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staffs = User::whereRoleIs('staff')->get();
        
        if(Auth::user()->hasRole('admin'))
            $hotels = Hotel::all();

        if(Auth::user()->hasRole('manager'))
            $hotels = Hotel::where('manager_id', Auth::user()->id )->get();

        return view('internal.staff.create', compact('staffs','hotels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $check = Staff::where('user_id',$request->user_id)->first();

        if($check) {
            $this->update($request);
            return redirect()->route('staff.show',$check->id)->withFlashSuccess('Staff Update Successfully');
        } else {
            $staff = New Staff;
            $staff->user_id = $request->user_id;
            $staff->hotel_id = $request->hotel_id;
            $staff->staff_position = $request->staff_position;
            $staff->save();

            activity()
            ->withProperties(['user' => $staff])
            ->log('New Staff Asigned');

            return redirect()->route('staff.show',$staff->id)->withFlashSuccess('Staff Asigned Successfully');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Staff::findOrFail($id);

        return view('internal.staff.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Staff::findOrFail($id);
        
        if(Auth::user()->hasRole('admin'))
            $hotels = Hotel::all();

        if(Auth::user()->hasRole('manager'))
            $hotels = Hotel::where('manager_id', Auth::user()->id )->get();

        return view('internal.staff.edit', compact('user','hotels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        if($id == null)
            $staff = Staff::where('user_id',$request->user_id)->first();
        else
            $staff = Staff::findOrFail($id);
        
        $staff->user_id = $request->user_id;
        $staff->hotel_id = $request->hotel_id;
        $staff->staff_position = $request->staff_position;
        $staff->save();

        activity()
        ->withProperties(['user' => $staff])
        ->log('Staff Asignment Updated');

        return redirect()->route('staff.show',$staff->id)->withFlashSuccess('Staff Update Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $staff = Staff::findOrFail($id);

        activity()
        ->withProperties(['user' => $staff])
        ->log('Staff Asignment Deleted');
        
        $staff->delete();

        if($staff)
            return response()->json(['message' => 'success'], 200);
        else
            return response()->json(['message' => 'Staff is being used'], 400);

    }
}

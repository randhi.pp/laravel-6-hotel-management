<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Hotel;
use App\Staff;
use App\User;
use Auth;


class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ( $user->hasRole('admin') )
           $hotels = Hotel::all();
        if ( $user->hasRole('manager') )
           $hotels = Hotel::where('manager_id',$user->id)->get();
        if ( $user->hasRole('staff') )
           $hotels = Staff::with('hotels')
                        ->where('user_id',$user->id)
                        ->get();

        return view('internal.hotel.index', compact('hotels'));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function apiIndex()
    {
        $user = auth('api')->user();

        if ( $user->hasRole('admin') )
           $hotels = Hotel::all();
        if ( $user->hasRole('manager') )
           $hotels = Hotel::where('manager_id',$user->id)->get();
        if ( $user->hasRole('staff') )
           $hotels = Staff::with('hotels')
                        ->where('user_id',$user->id)
                        ->get();

        return response()->json([
            'data' => $hotels
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $managers = User::whereRoleIs('manager')->get();

        return view('internal.hotel.create', compact('managers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        //get 1st char on each word
        $words = explode(" ", $request->name);
        $acronym = "";
        foreach ($words as $w)
            $acronym .= $w[0];

        $hotel = new Hotel;
        $hotel->code = $acronym;
        $hotel->name = $request->name;
        $hotel->manager_id = $request->manager_id;
        $hotel->phone = $request->phone;
        $hotel->address = $request->address;

        if($request->photo) {
            $file = $request->file('photo');
            $path = $file->store('images','s3','public');
            $hotel->image_url = $path;
        }

        $hotel->save();

        return redirect()->route('hotel.index')->withFlashSuccess('Hotel Saved.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $managers = User::whereRoleIs('manager')->get();
        $hotel = Hotel::findOrFail($id);

        if ( $user->hasRole('manager') )
            if( $hotel->manager_id !== $user->id)
                return abort(403,'Hotel not managed by you');

        if ( $user->hasRole('staff') )
            return abort(403,'You Are Not Assigned to this Hotel');
        //    $hotels = Staff::where('user_id',$user->id)
        //                 ->get('hotel_id')->toArray();
        //     if( inArray( $hotels, $user->id ) == FALSE )



        return view('internal.hotel.view', compact('hotel','managers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $managers = User::whereRoleIs('manager')->get();
        $hotel = Hotel::findOrFail($id);

        if ( $user->hasRole('manager') )
            if( $hotel->manager_id !== $user->id)
                return abort(403,'Hotel not managed by you');

        if ( $user->hasRole('staff') )
            return abort(403,'You Are Not Assigned to this Hotel');
        //    $hotels = Staff::where('user_id',$user->id)
        //                 ->get('hotel_id')->toArray();
        //     if( inArray( $hotels, $user->id ) == FALSE )



        return view('internal.hotel.edit', compact('hotel','managers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        $hotel = Hotel::findOrFail($id);
        
        $hotel->name = $request->name;
        $hotel->phone = $request->phone;
        $hotel->address = $request->address;

        if(isset($request->manager_id))
            $hotel->manager_id = $request->manager_id;

        if($request->photo) {
            $file = $request->file('photo');
            $path = $file->store('images','s3','public');
            $hotel->image_url = $path;
        }

        $hotel->save();

        return redirect()->route('hotel.index')->withFlashSuccess('Hotel Updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotel = Hotel::findOrFail($id)->delete();
        if($hotel)
            return response()->json(['message' => 'success'], 200);
        else
            return response()->json(['message' => 'Hotel is being used'], 400);

    }
}

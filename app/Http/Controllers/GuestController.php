<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;

class GuestController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hotels = Hotel::all();

        return view('welcome',compact('hotels'));
    }
}

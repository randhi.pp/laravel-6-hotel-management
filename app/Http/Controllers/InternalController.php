<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Hotel;
use Auth;

class InternalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hotels = Hotel::where('manager_id', Auth::user()->id )->get();

        return view('internal.home',compact('hotels'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

use App\Notifications\UserCreated;

use App\User;
use App\Role;
use App\Permission;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        // dd($request);
        $user = new User;
        $user->name = $request->Name;
        $user->email = $request->Email;
        $user->phone = $request->Phone;
        $user->address = $request->Address;
        $user->password = Hash::make($request->password);


        if($request->photo) {
            $file = $request->file('photo');
            $path = $file->store('images/profiles','s3','public');
            $user->image_url = $path;
        }

        $user->save();

        foreach($request->roles as $role)
            $user->attachRole($role);

        return redirect()->route('home')->withFlashSuccess('User Saved.');   
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function apiStore(Request $request)
    {
        //dd($request);
        $faker = Faker::create();
        $password = $faker->password;

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->password = Hash::make($password);

        $user->save();

        $user->notify(new UserCreated($user,$password));

        $user->attachRole($request->role);

        return response()->json([
            'data' => $user,
            'message' => 'User Created Successfully'
        ], 200);    
         
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function userIndex()
    {
        $users = User::all();

        return view('internal.user.index', compact('users'));
    }
    public function userView($id = null)
    {
        if(!$id)
            $id = Auth::user()->id;

        $user = User::findOrFail($id);
        $roles = Role::all();
        $permissions = Permission::all();

        return view('internal.user.view', compact('user','roles','permissions'));
    }
    public function userCreate()
    {
        $roles = Role::all();
        $permissions = Permission::all();
        return view('internal.user.create', compact('roles','permissions'));
    }
    public function userUpdate(Request $request, $id = null)
    {
        // dd($request);
        
        if(!$id)
            $id = Auth::user()->id;

        // dd($request);
        $user = User::findOrFail($id);

        $user->name = $request->Name;
        $user->email = $request->Email;
        $user->phone = $request->Phone;
        $user->address = $request->Address;

        if($request->photo) {
            $file = $request->file('photo');
            $path = $file->store('images/profiles','s3','public');
            $user->image_url = $path;
        }

        if($request->roles) {
            
            $existing = $user->getRoles();
            
            $user->detachRoles($existing);

            foreach($request->roles as $role)
                $user->attachRole($role);
        
        }

        $user->save();

        if ( $user->hasRole('member') )
            return redirect()->route('member.home')->withFlashSuccess('Model berhasil disimpan.');
        else
            return redirect()->route('home')->withFlashSuccess('Model berhasil disimpan.');

        // return view('internal.user.update', compact('users'));
    }
    public function userEdit($id = null)
    {
        if(!$id)
            $id = Auth::user()->id;

        $user = User::findOrFail($id);
        $roles = Role::all();
        $permissions = Permission::all();
        return view('internal.user.edit', compact('user','roles','permissions'));
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($user) {
            $user = $user->delete();
            return response()->json(['message' => 'success'], 200);
        } else
            return response()->json(['message' => 'Error Unknown'], 400);

    }
}

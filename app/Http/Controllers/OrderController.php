<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Hotel;
use App\Room;
use App\Staff;
use App\User;
use App\RoomType;


use Auth;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('admin'))
            $orders = Order::paginate(20);

        if(Auth::user()->hasRole('manager')) {
            $hotels = Hotel::where('manager_id', Auth::user()->id )->get()->toArray();

            foreach ($hotels as $key => $value) {
                $hotelArray[] = $value['id'];
            }

            $orders = Order::whereIn('hotel_id',$hotelArray)->paginate(20);

        }
        if(Auth::user()->hasRole('staff')) {
            $hotel = Staff::where('user_id', Auth::user()->id )->first();

            if(!$hotel)
                return redirect()->route('home')->withFlashError('Not Asigned to any Hotel');

            $orders = Order::where('hotel_id',$hotel->hotel_id)->paginate(20);
        }
        if(Auth::user()->hasRole('member')) {
            $orders = Order::where('customer_id',Auth::user()->id)->paginate(20);
        }

        return view('internal.order.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $hotels = Hotel::all();
        $type = RoomType::all();
        $users = User::whereRoleIs('member')->get();
        // dd($type);

        return view('internal.order.create', compact('users','hotels','type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $order = New Order;

        if(Auth::user()->hasRole('member'))
            $order->customer_id = Auth::user()->id;
        else
            $order->customer_id = $request->customer_id;

        $order->hotel_id = $request->hotel_id;
        $order->room_id = $request->room_id;
        $order->book_start = Carbon::parse($request->book_start)->toDateTimeString();
        $order->book_end = Carbon::parse($request->book_end)->toDateTimeString();

        $order->save();

        activity()
        ->withProperties(['order' => $order])
        ->log('New Booking Order');

        if(Auth::user()->hasRole('member'))
            return redirect()->route('booking.index')->withFlashSuccess('Booking Order Created');
        else
            return redirect()->route('order.index')->withFlashSuccess('Booking Order Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);

        if(Auth::user()->hasRole('member')) {
            $customer = Auth::user();
            if( $customer->id !== $order->customer_id )
                return redirect()->route('booking.index')->withFlashError('Not Your Booking Order');
        }

        return view('internal.order.view', compact('order'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);

        if(Auth::user()->hasRole('member')) {
            $customer = Auth::user();
            if( $customer->id !== $order->customer_id )
                return redirect()->route('booking.index')->withFlashError('Not Your Booking Order');
        }

        $hotels = Hotel::all();
        $type = RoomType::all();

        // dd($type);

        return view('internal.order.edit', compact('order','hotels','type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);

        $order = Order::findOrFail($id);

        if(Auth::user()->hasRole('member')) {
            $customer = Auth::user();
            if( $customer->id !== $order->customer_id )
                return redirect()->route('booking.index')->withFlashError('Not Your Booking Order');
        }

        $order->hotel_id = $request->hotel_id;
        $order->room_id = $request->room_id;
        $order->book_start = Carbon::parse($request->book_start)->toDateTimeString();
        $order->book_end = Carbon::parse($request->book_end)->toDateTimeString();

        $order->save();

        activity()
        ->withProperties(['order' => $order])
        ->log('Order Updated');

        if(Auth::user()->hasRole('member'))
            return redirect()->route('booking.index')->withFlashSuccess('Booking Order Updated');
        else
            return redirect()->route('order.index')->withFlashSuccess('Booking Order Updated');
        // dd($room);

    //   "user_id" => "64"
    //   "name" => "Member"
    //   "hotel_id" => "8"
    //   "room" => "studio"
    //   "book_start" => "02/05/2020"
    //   "book_end" => "02/07/2020"
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);

        if(Auth::user()->hasRole('member')) {
            $customer = Auth::user();
            if( $customer->id !== $order->customer_id )
                return redirect()->route('booking.index')->withFlashError('Not Your Booking Order');
        }

        activity()
        ->withProperties(['order' => $order])
        ->log('Order Deleted');

        $order->delete();

        if($order)
            return response()->json(['message' => 'success'], 200);
        else
            return response()->json(['message' => 'Order is being used'], 400);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

use App\Notifications\UserCreated;

use App\User;
use App\Role;
use App\Permission;
use Auth;

class APIUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = auth('api')->user();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        //dd($request);
        if(!$this->user->hasRole(['admin','manager']))
            return response()->json([
                'error' => 'Forbidden',
                'message' => 'User Restricted'
            ], 403);  

        $check = User::where('email',$request->email)->first();

        if($check)
            return response()->json([
                'error' => 'Duplicate',
                'message' => 'User Exist in database'
            ], 403);  


        $faker = Faker::create();
        $password = $faker->password;

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->password = Hash::make($password);

        $user->save();

        $user->notify(new UserCreated($user,$password));

        $user->attachRole($request->role);

        return response()->json([
            'data' => $user,
            'message' => 'User Created Successfully'
        ], 200);    
         
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {
        //dd($request);
        if(!$this->user->hasRole(['admin','manager']))
            return response()->json([
                'error' => 'Forbidden',
                'message' => 'User Restricted'
            ], 403);  
        
        $user = User::where('email',$request->email)->first();

        $data = $request->except(['password','token','role']);


        foreach ($data as $key => $value) {
            
            // dd($user->$key);

            if(!empty($value))
                $user->$key = $value;
        }
       
        // $user->name = $request->name;
        // $user->email = $request->email;
        // $user->phone = $request->phone;
        // $user->address = $request->address;
        if($request->password)
            $user->password = Hash::make($request->password);

        $user->save();

        // $user->notify(new UserCreated($user,$password));

        if($request->role) {
            $existing = $user->getRoles();
            $user->detachRoles($existing);
            $user->attachRole($request->role);

        }
            

        return response()->json([
            'data' => $user,
            'message' => 'User Update Successfully'
        ], 200);    
         
    }

    public function show(Request $request)
    {
        
        if(!$this->user->hasRole(['admin','manager']))
            return response()->json([
                'error' => 'Forbidden',
                'message' => 'User Restricted'
            ], 403);  


        $user = User::with('roles','permissions')->findOrFail($request->id);
        
        return response()->json([
            'data' => $user,
            'message' => 'User Query Successfully'
        ], 200); 
    }
   

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->user->hasRole(['admin']))
            return response()->json([
                'error' => 'Forbidden',
                'message' => 'User Restricted'
            ], 403);  
        
        $user = User::find($id);
        if($user) {
            $user = $user->delete();
            return response()->json(['data' => 'User Deleted Successfully'], 200);
        } else
            return response()->json(['error' => 'User Not Found or Already Deleted'], 400);

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;


class Staff extends Model
{
    use Userstamps;
    use LogsActivity;
    use SoftDeletes;

    protected $fillable = [
        'user_id','hotel_id','staff_position'
    ];

    protected static $logFillable = true;

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }


}

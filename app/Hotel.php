<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;


class Hotel extends Model
{
    use Userstamps;
    use LogsActivity;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'address', 'phone'
    ];

    protected static $logFillable = true;

    public function rooms()
    {
        return $this->hasMany('App\Room');
    }
    public function staffs()
    {
        return $this->hasMany('App\Staff');
    }
    public function manager()
    {
        return $this->belongsTo('App\User', 'manager_id', 'id');
    }
}

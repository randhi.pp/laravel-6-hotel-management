@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Halo Member
                    You are logged in as {{ Auth::user()->name }}
                    @role('admin')
                        <p>This is visible to users with the admin role. Gets translated to
                        \Laratrust::hasRole('admin')</p>
                    @endrole
                    @role('manager')
                        <p>This is visible to users with the manager role. Gets translated to
                        \Laratrust::hasRole('manager')</p>
                    @endrole
                    @role('staff')
                        <p>This is visible to users with the staff role. Gets translated to
                        \Laratrust::hasRole('staff')</p>
                    @endrole
                    @role('member')
                        <p>This is visible to users with the member role. Gets translated to
                        \Laratrust::hasRole('member')</p>
                    @endrole
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

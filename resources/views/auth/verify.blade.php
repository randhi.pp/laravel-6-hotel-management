@extends('layouts.backend')

@push('before-scripts')
    <style>
        .bg-login-image {
            background: url("/images/1.jpg");
                background-position-x: 0%;
                background-position-y: 0%;
                background-size: auto;
            background-position: center;
            background-size: cover;
        }
    </style>
@endpush

@section('content')
<div class="container">

    <!-- Outer Row -->
    {{-- <div class="row justify-content-center p-2"> --}}

        

            <div class="card o-hidden border-0 shadow-lg my-5 h-100" >
                <div class="card-body p-0 h-100">
                    <div class="row">
                        <div class="col-lg-5 d-none d-lg-block bg-login-image" style="height: 400px;"></div>
                        <div class="col-lg-7 p-5">
                            <h2>Email Verification Needed</h2>
                            
                            {{ __('Before proceeding, please check your email for a verification link.') }}<br>
                            {{ __('If you did not receive the email') }},
                            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                @csrf
                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                            </form>

                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="text-center p-0">
                <a class="small text-light" href="{{ route('guest') }}">Back to Home</a>
            </div>
    {{-- </div> --}}
</div>
@endsection

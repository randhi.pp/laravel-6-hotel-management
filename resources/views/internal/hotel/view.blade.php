@extends('layouts.backend')

@section('content')
<div class="container-fluid">

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Hotel Detail</h6>
    </div>
    <div class="card-body">
        <div class="row mt-4 mb-4">
          <div class="col">
            <div class="form-group row">
              {{ html()->label('Name')->class('col-md-2 form-control-label')->for('name') }}
                <div class="col-md-10">
                    {{ html()->text('name')
                        ->class('form-control')
                        ->placeholder('name')
                        ->attribute('maxlength', 191)
                        ->value($hotel->name)
                        ->readonly() }}
                </div><!--col-->
            </div><!--form-group-->
            
            <div class="form-group row">
              {{ html()->label('Address')->class('col-md-2 form-control-label')->for('address') }}
                  <div class="col-md-10">
                      {{ html()->text('address')
                          ->class('form-control')
                          ->placeholder('address')
                          ->attribute('maxlength', 191)
                          ->value($hotel->address)
                         }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Phone')->class('col-md-2 form-control-label')->for('phone') }}

                  <div class="col-md-10">
                      {{ html()->text('phone')
                          ->class('form-control')
                          ->placeholder('phone')
                          ->attribute('maxlength', 191)
                          ->value($hotel->phone)
                          ->readonly() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('About')->class('col-md-2 form-control-label')->for('about') }}

                  <div class="col-md-10">
                      {{ html()->textarea('about')
                          ->class('form-control')
                          ->placeholder('about')
                          ->attribute('maxlength', 191)
                          ->value($hotel->about)
                          }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="row">

            </div>
            <div class="form-group row">
                {{ html()->label('Photo')->class('col-md-2 form-control-label')->for('photo') }}

                    <div class="col-md-10">
                        <img class="img-profile rounded-circle mb-2" style="object-fit: cover;border-radius: 50%;
                    height: 200px;
                    width: 200px;" src="{{ $hotel->image_url ? Storage::disk('s3')->url($hotel->image_url) : Storage::disk('s3')->url('images/profiles/default.png') }}">
                        {{-- {{ html()->file('photo')
                            ->class('form-control') }} --}}
                    </div><!--col-->
              </div><!--form-group-->

          </div>
        </div>

    </div>
    
  </div>
</div>
@endsection

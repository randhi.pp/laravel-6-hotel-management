@extends('layouts.backend')

@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Hotel List</h1>
  <p class="mb-4">Admin can list all internal user and member list.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Hotel List
      <a href="{{ route('hotel.create') }}" type="button" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus"></i> Add Hotel</a></h6>

    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-sm">
          <thead class="thead">
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Hotel Code</th>
              <th class="text-center">Hotel Name</th>
              <th class="text-center" >Total Room</th>
              <th class="text-center">Manager</th>
              <th class="text-center">Hotel Address</th>
              <th class="text-center">Phone</th>
              <th class="text-center">Last Update</th>
              <th ></th>


            </tr>
          </thead>
          <tbody>
            @foreach ($hotels as $key => $hotel)
              <tr>
                <td class="text-center">{{ $key+1 }}</td>
                <td>{{ $hotel->code }}</td>
                <td>{{ $hotel->name }}</td>
                <td class="text-center">{{ $hotel->rooms()->count() }}</td>
                <td>{{ $hotel->manager->name }}</td>
                <td>{{ $hotel->address }}</td>
                <td>{{ $hotel->phone }}</td>
                <td>{{ $hotel->updated_at->diffForHumans() }}</td>
                <td >
                  <div class="btn-group" role="group" aria-label="User Actions">
                    <a href="{{ route('hotel.show', $hotel->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="View">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('hotel.edit', $hotel->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-primary" data-original-title="Edit">
                        <i class="fas fa-edit"></i>
                    </a>
                    <button type="button" id="btnDelete" data-id="{{$hotel->id}}" data-name="{{$hotel->name}}"  class="btn btn-danger btnDelete" >
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@push('after-scripts')
<script>
$(document).ready(function() {
    $('.btnDelete').each(function(index) {
        $(this).on("click", function(){
            var hotel_id = $(this).data('id');
            var hotel = $(this).data('name');
            var content = document.createElement('div');
                content.innerHTML = 'Anda yakin ingin hapus hotel ini? <code>'+hotel+'</code>';

            Swal.fire({
                // position: 'bottom-end',
                icon: 'error',
                title: 'Delete a Hotel?',
                html: content,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                // toast: true,
                // timer: 3000,
                // showConfirmButton: false,

                // footer: '<a href>Why do I have this issue?</a>'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "DELETE",
                        url: "hotel/"+hotel_id,

                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                            'Deleted!',
                            'Your hotel has been deleted.',
                            'success'
                            ).then(function() {
                                window.location.reload(1);
                            });

                        },
                        error: function (response) {
                            console.log(response.responseJSON.message);
                            Swal.fire(
                            'Error!',
                            response.responseJSON.message,
                            'error'
                            );
                        }
                    })

                }
                })
            });
        });
});
</script>
@endpush

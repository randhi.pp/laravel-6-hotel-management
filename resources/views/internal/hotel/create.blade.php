@extends('layouts.backend')

@section('content')
<div class="container-fluid">

{{ html()->Form('POST', route('hotel.store'))->class('form-horizontal')->acceptsFiles()->open() }}
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Add Hotel</h6>
    </div>
    <div class="card-body">
        <div class="row mt-4 mb-4">
          <div class="col">
            <div class="form-group row">
              {{ html()->label('Hotel Name')->class('col-md-2 form-control-label')->for('name') }}
                <div class="col-md-10">
                    {{ html()->text('name')
                        ->class('form-control')
                        ->placeholder('Please Add Hotel Name')
                        ->attribute('maxlength', 191)

                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            @role('admin')
            <div class="form-group row">
              {{ html()->label('Manager')->class('col-md-2 form-control-label')->for('manager') }}
                <div class="col-md-10">
                    {{ html()->select('manager_id', $managers->pluck('name','id'))
                        ->class('form-control')

                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            @endrole
            <div class="form-group row">
              {{ html()->label('Address')->class('col-md-2 form-control-label')->for('address') }}
                  <div class="col-md-10">
                      {{ html()->text('address')
                          ->class('form-control')
                          ->placeholder('Address')
                          ->attribute('maxlength', 191)

                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Phone')->class('col-md-2 form-control-label')->for('phone') }}

                  <div class="col-md-10">
                      {{ html()->text('phone')
                          ->class('form-control')
                          ->placeholder('Hotel Phone')
                          ->attribute('maxlength', 191)

                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('About')->class('col-md-2 form-control-label')->for('about') }}

                  <div class="col-md-10">
                      {{ html()->textarea('about')
                          ->class('form-control')
                          ->placeholder('Hotel Quick Review')
                          ->attribute('maxlength', 191)

                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="row">

            </div>
            <div class="form-group row">
                {{ html()->label('Photo')->class('col-md-2 form-control-label')->for('photo') }}

                    <div class="col-md-10">

                        {{ html()->file('photo')
                            ->class('form-control') }}
                    </div><!--col-->
              </div><!--form-group-->

          </div>
        </div>

    </div>
    <div class="card-footer">
      <div class="row">
          <div class="col">
              <a href="{{ route('hotel.index') }}" class="btn btn-danger btn-sm">Cancel</a>
          </div><!--col-->

          <div class="col text-right">
            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Update</button>
              {{-- {{ form_submit('Update') }} --}}
          </div><!--row-->
      </div><!--row-->
    </div><!--card-footer-->
  </div>
  {{ html()->form()->close() }}
</div>
@endsection

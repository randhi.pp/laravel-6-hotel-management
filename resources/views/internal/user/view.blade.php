@extends('layouts.backend')

@section('content')
<div class="container-fluid">
{{-- @role('member')
    {{ html()->modelForm($user, 'PATCH', route('member.user.update'))->class('form-horizontal')->acceptsFiles()->open() }}
@else
    {{ html()->modelForm($user, 'PATCH', route('user.update'))->class('form-horizontal')->acceptsFiles()->open() }}
@endrole --}}
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">User Detail</h6>
    </div>
    <div class="card-body">
        <div class="row mt-4 mb-4">
          <div class="col-md-6">
            <div class="form-group row">
                

                    <div class="col-md-10">
                        <img class="img-profile rounded-circle mb-2" style="object-fit: cover;border-radius: 50%;
                    height: 200px;
                    width: 200px;" src="{{ $user->image_url ? Storage::disk('s3')->url($user->image_url) : Storage::disk('s3')->url('images/profiles/default.png') }}">
                      
                    </div><!--col-->
              </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Name')->class('col-md-2 form-control-label')->for('name') }}
                <div class="col-md-10">
                    {{ html()->text('Name')
                        ->class('form-control')
                        ->placeholder('name')
                        ->attribute('maxlength', 191)
                        ->value($user->name)
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Email')->class('col-md-2 form-control-label')->for('email') }}
                  <div class="col-md-10">
                      {{ html()->email('Email')
                          ->class('form-control')
                          ->placeholder('email')
                          ->attribute('maxlength', 191)
                          ->value($user->email)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Phone')->class('col-md-2 form-control-label')->for('phone') }}

                  <div class="col-md-10">
                      {{ html()->text('Phone')
                          ->class('form-control')
                          ->placeholder('phone')
                          ->attribute('maxlength', 191)
                          ->value($user->phone)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Address')->class('col-md-2 form-control-label')->for('address') }}

                  <div class="col-md-10">
                      {{ html()->text('Address')
                          ->class('form-control')
                          ->placeholder('address')
                          ->attribute('maxlength', 191)
                          ->value($user->address)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="row">

            </div>
            
            @if($user->id != 1)

            <div class="form-group row">
              {{ html()->label('Abilities')->class('col-md-2 form-control-label') }}
              <div class="col-md-6">

                                  @if($roles->count())
                                      @foreach($roles as $role)
                                        @if(in_array($role->name, $user->getRoles()))
                                          <div class="card mt-2 mb-2">
                                              <div class="card-header">
                                                <div class="custom-control custom-switch">
                                                <input type="checkbox" disabled name="roles[]" class="custom-control-input" {{ in_array($role->name, $user->getRoles()) ? 'checked ' : '' }} id="role-{{$role->id}}" value="{{$role->id}}">
                                                <label class="custom-control-label" for="role-{{$role->id}}">{{ ucwords($role->name) }}</label>
                                                </div>
                                              </div>
                                              <div class="card-body">
                                                  @if($role->id != 1)
                                                      @if($role->permissions->count())
                                                          @foreach($role->permissions as $permission)
                                                              <i class="fas fa-dot-circle"></i> {{ ucwords($permission->name) }}
                                                          @endforeach
                                                      @else
                                                          None
                                                      @endif
                                                  @else
                                                      All Permission
                                                  @endif
                                              </div>
                                          </div><!--card-->
                                          @endif
                                      @endforeach
                                  @endif

              </div>
            </div><!--form-group-->

            @else
            @role('admin')
            <div class="form-group row">
              {{ html()->label('Abilities')->class('col-md-2 form-control-label') }}
              <div class="col-md-6">

                                  @if($roles->count())
                                      @foreach($roles->take(1) as $role)
                                          <div class="card mt-2 mb-2">
                                              <div class="card-header">
                                                <div class="custom-control custom-switch">
                                                <input type="checkbox" disabled name="roles[]" disabled class="custom-control-input" {{ in_array($role->name, $user->getRoles()) ? 'checked ' : '' }} id="role-{{$role->id}}">
                                                <label class="custom-control-label" for="role-{{$role->id}}">{{ ucwords($role->name) }}</label>
                                                </div>
                                              </div>
                                              <div class="card-body">
                                                  @if($role->id != 1)
                                                      @if($role->permissions->count())
                                                          @foreach($role->permissions as $permission)
                                                              <i class="fas fa-dot-circle"></i> {{ ucwords($permission->name) }}
                                                          @endforeach
                                                      @else
                                                          None
                                                      @endif
                                                  @else
                                                      All Permission
                                                  @endif
                                              </div>
                                          </div><!--card-->
                                      @endforeach
                                  @endif

              </div>
            </div><!--form-group-->
            @endrole
            @endif
          </div>
        </div>

    </div>
   
  </div>
  {{-- {{ html()->closeModelForm() }} --}}
</div>
@endsection

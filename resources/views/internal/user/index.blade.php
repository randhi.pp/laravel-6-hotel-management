@extends('layouts.backend')

@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">User List</h1>
  <p class="mb-4">Admin can list all internal user and member list.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-md-6">
                <h6 class="m-0 font-weight-bold text-primary">User List</h6>
            </div>
            <div class="col-md-6">
                <a href="{{ route('user.create') }}" class="btn btn-sm btn-success float-right"><i class="fa fa-plus"></i> Add User</a>
            </div>
        </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-sm">
          <thead class="thead">
            <tr>
              <th class="text-center">#</th>
              <th class="text-center" >User Name</th>
              <th class="text-center">Phone</th>
              <th class="text-center">Email</th>
              <th class="text-center">Confirmed</th>
              <th class="text-center">Role</th>
              <th class="text-center">Last Update</th>
              <th ></th>


            </tr>
          </thead>
          <tbody>
            @foreach ($users as $key => $user)
              <tr>
                <td class="text-center">{{ $key+1 }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->email }}</td>
                <td class="text-center">{!! $user->email_verified_at?'<span class="badge badge-success">yes</span>':'<span class="badge badge-danger">no</span>' !!}</td>
                <td>
                  @foreach ($user->roles as $role)
                  {{ $role->name }}{{ $user->roles->count() > 1 ? ',' : '' }}
                  @endforeach
                </td>
                <td>{{ $user->updated_at->diffForHumans() }}</td>
                <td >
                  <div class="btn-group" role="group" aria-label="User Actions">
                    <a href="{{ route('user.view', $user->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="View">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('user.edit', $user->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-primary" data-original-title="Edit">
                        <i class="fas fa-edit"></i>
                    </a>
                    <button type="button" id="btnDelete" data-id="{{$user->id}}" data-name="{{$user->name}}"  class="btn btn-danger btnDelete" >
                      <i class="fas fa-times"></i>
                  </button>
                </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@push('after-scripts')
<script>
$(document).ready(function() {
    $('.btnDelete').each(function(index) {
        $(this).on("click", function(){
            var id = $(this).data('id');
            var name = $(this).data('name');
            var content = document.createElement('div');
                content.innerHTML = 'Anda yakin ingin hapus user ini? <code>'+name+'</code>';

            Swal.fire({
                // position: 'bottom-end',
                icon: 'error',
                title: 'Delete User?',
                html: content,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                // toast: true,
                // timer: 3000,
                // showConfirmButton: false,

                // footer: '<a href>Why do I have this issue?</a>'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "DELETE",
                        url: "/admin/user/"+id+"/delete",

                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                            'Deleted!',
                            'Your User  has been deleted.',
                            'success'
                            ).then(function() {
                                window.location.reload(1);
                            });

                        },
                        error: function (response) {
                            console.log(response.responseJSON.message);
                            Swal.fire(
                            'Error!',
                            response.responseJSON.message,
                            'error'
                            );
                        }
                    })

                }
                })
            });
        });
});
</script>
@endpush

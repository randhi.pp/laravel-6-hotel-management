@extends('layouts.backend')

@section('content')
<div class="container-fluid">

{{ html()->form('POST', route('user.store'))->class('form-horizontal')->acceptsFiles()->open() }}
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Add New User / Staff / Manager</h6>
    </div>
    <div class="card-body">
        <div class="row mt-4 mb-4">
          <div class="col">
            <div class="form-group row">
              {{ html()->label('Name')->class('col-md-2 form-control-label')->for('name') }}
                <div class="col-md-10">
                    {{ html()->text('Name')
                        ->class('form-control')
                        ->placeholder('name')
                        ->attribute('maxlength', 191)
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Email')->class('col-md-2 form-control-label')->for('email') }}
                  <div class="col-md-10">
                      {{ html()->email('Email')
                          ->class('form-control')
                          ->placeholder('email')
                          ->attribute('maxlength', 191)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
                {{ html()->label('Password')->class('col-md-2 form-control-label')->for('password') }}
                    <div class="col-md-10">
                        {{ html()->password('password')
                            ->class('form-control')
                            ->placeholder('Min 6 variable character')
                            // ->attribute('minlength', 6)
                            ->required() }}
                    </div><!--col-->
              </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Phone')->class('col-md-2 form-control-label')->for('phone') }}

                  <div class="col-md-10">
                      {{ html()->text('Phone')
                          ->class('form-control')
                          ->placeholder('phone')
                          ->attribute('maxlength', 191)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Address')->class('col-md-2 form-control-label')->for('address') }}

                  <div class="col-md-10">
                      {{ html()->text('Address')
                          ->class('form-control')
                          ->placeholder('address')
                          ->attribute('maxlength', 191)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="row">

            </div>
            <div class="form-group row">
                {{ html()->label('Photo')->class('col-md-2 form-control-label')->for('photo') }}

                    <div class="col-md-10">
                        {{ html()->file('photo')
                            ->class('form-control') }}
                    </div><!--col-->
              </div><!--form-group-->
            @role('admin')
            <div class="form-group row">
              {{ html()->label('Abilities')->class('col-md-2 form-control-label') }}
              <div class="col-md-6">

                                  @if($roles->count())
                                      @foreach($roles as $key => $role)
                                          <div class="card mt-2 mb-2">
                                              <div class="card-header">
                                                <div class="custom-control custom-switch">
                                                <input type="checkbox" name="roles[]" value="{{ $role->id }}" class="custom-control-input" id="role-{{$key}}" >
                                                <label class="custom-control-label" for="role-{{$key}}">{{ ucwords($role->name) }}</label>
                                                </div>
                                              </div>
                                              <div class="card-body">
                                                  @if($role->id != 1)
                                                      @if($role->permissions->count())
                                                          @foreach($role->permissions as $permission)
                                                              <i class="fas fa-dot-circle"></i> {{ ucwords($permission->name) }}
                                                          @endforeach
                                                      @else
                                                          None
                                                      @endif
                                                  @else
                                                      All Permission
                                                  @endif
                                              </div>
                                          </div><!--card-->
                                      @endforeach
                                  @endif

              </div>
            </div><!--form-group-->
            @endrole

          </div>
        </div>

    </div>
    <div class="card-footer">
      <div class="row">
          <div class="col">
              <a href="{{ route('user.index') }}" class="btn btn-danger btn-sm">Cancel</a>
          </div><!--col-->

          <div class="col text-right">
            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Update</button>
              {{-- {{ form_submit('Update') }} --}}
          </div><!--row-->
      </div><!--row-->
    </div><!--card-footer-->
  </div>
  {{ html()->closeModelForm() }}
</div>
@endsection

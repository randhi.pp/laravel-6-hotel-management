@extends('layouts.backend')

@section('content')
<div class="container-fluid">

{{ html()->form('POST', route('staff.store'))->class('form-horizontal')->open() }}
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Assign Staf to a Hotel</h6>
    </div>
    <div class="card-body">
        <div class="row mt-4 mb-4">
          <div class="col">
            <div class="form-group row">
              {{ html()->label('Name')->class('col-md-2 form-control-label')->for('user_id') }}
                <div class="col-md-10">
                    {{ html()->select('user_id', $staffs->pluck('name','id'))
                        ->class('form-control')
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Hotel')->class('col-md-2 form-control-label')->for('hotel_id') }}
                <div class="col-md-10">
                    {{ html()->select('hotel_id', $hotels->pluck('name','id'))
                        ->class('form-control')
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Position')->class('col-md-2 form-control-label')->for('staff_position') }}
                <div class="col-md-10">
                    {{ html()->text('staff_position')
                        ->class('form-control')
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
          </div>
        </div>

    </div>
    <div class="card-footer">
      <div class="row">
          <div class="col">
              <a href="{{ route('staff.index') }}" class="btn btn-danger btn-sm">Cancel</a>
          </div><!--col-->

          <div class="col text-right">
            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Update</button>
              {{-- {{ form_submit('Update') }} --}}
          </div><!--row-->
      </div><!--row-->
    </div><!--card-footer-->
  </div>
  {{ html()->form()->close() }}
</div>
@endsection

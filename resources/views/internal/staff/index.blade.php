@extends('layouts.backend')

@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Staff List</h1>
  <p class="mb-4">Admin / Manager can list all staff and assign to a hotel.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Staff List
      <a href="{{ route('staff.create') }}" type="button" class="btn btn-sm btn-primary float-right"><i class="fa fa-sign"></i> Assign Staff</a></h6>

    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-sm">
          <thead class="thead">
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Name</th>
              <th class="text-center">Position</th>
              <th class="text-center">Assigned to</th>
              <th class="text-center">Last Update</th>
              <th ></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($staffs as $key => $staff)
              <tr>
                <td class="text-center">{{ $key+1 }}</td>
                <td>{{ $staff->user->name }}</td>
                <td>{{ $staff->staff_position }}</td>
                <td>{{ $staff->hotel->name }}</td>
                <td>{{ $staff->updated_at->diffForHumans() }}</td>
                <td >
                  <div class="btn-group" role="group" aria-label="User Actions">
                    <a href="{{ route('staff.show', $staff->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="View">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('staff.edit', $staff->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-primary" data-original-title="Edit">
                        <i class="fas fa-edit"></i>
                    </a>
                    <button type="button" id="btnDelete" data-id="{{$staff->id}}" data-name="{{$staff->name}}"  class="btn btn-danger btnDelete" >
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {{ $staffs->links()  }}
      </div>
    </div>
  </div>

</div>
@endsection
@push('after-scripts')
<script>
$(document).ready(function() {
    $('.btnDelete').each(function(index) {
        $(this).on("click", function(){
            var id = $(this).data('id');
            var name = $(this).data('name');
            var content = document.createElement('div');
                content.innerHTML = 'Anda yakin ingin hapus Staff dari Hotel? <code>'+name+'</code>';

            Swal.fire({
                // position: 'bottom-end',
                icon: 'error',
                title: 'Delete asigned Staff? Staff Detail Still Remain.',
                html: content,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                // toast: true,
                // timer: 3000,
                // showConfirmButton: false,

                // footer: '<a href>Why do I have this issue?</a>'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "DELETE",
                        url: "staff/"+id,

                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                            'Deleted!',
                            'Your Staff Asignment has been deleted.',
                            'success'
                            ).then(function() {
                                window.location.reload(1);
                            });

                        },
                        error: function (response) {
                            console.log(response.responseJSON.message);
                            Swal.fire(
                            'Error!',
                            response.responseJSON.message,
                            'error'
                            );
                        }
                    })

                }
                })
            });
        });
});
</script>
@endpush

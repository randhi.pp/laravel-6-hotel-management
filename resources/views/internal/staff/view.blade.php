@extends('layouts.backend')

@section('content')
<div class="container-fluid">
{{-- @role('member')
    {{ html()->modelForm($user, 'PATCH', route('member.user.update'))->class('form-horizontal')->acceptsFiles()->open() }}
@else
    {{ html()->modelForm($user, 'PATCH', route('user.update'))->class('form-horizontal')->acceptsFiles()->open() }}
@endrole --}}
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">User Detail</h6>
    </div>
    <div class="card-body">
        <div class="row mt-4 mb-4">
          <div class="col-md-6">
            <div class="form-group row">
                

                    <div class="col-md-10">
                        <img class="img-profile rounded-circle mb-2" style="object-fit: cover;border-radius: 50%;
                    height: 200px;
                    width: 200px;" src="/storage/{{ $user->user->image_url ? $user->user->image_url : 'images/profiles/default.png' }}">
                      
                    </div><!--col-->
              </div><!--form-group-->
            <div class="form-group row">
            {{ html()->label('Asigned to Hotel')->class('col-md-2 form-control-label')->for('hotel') }}
                <div class="col-md-10">
                    {{ html()->text('Asigned to Hotel')
                        ->class('form-control')
                        ->value($user->hotel->name)
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Position')->class('col-md-2 form-control-label')->for('hotel') }}
                  <div class="col-md-10">
                      {{ html()->text('Position')
                          ->class('form-control')
                          ->value($user->staff_position)
                          ->required() }}
                  </div><!--col-->
              </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Name')->class('col-md-2 form-control-label')->for('name') }}
                <div class="col-md-10">
                    {{ html()->text('Name')
                        ->class('form-control')
                        ->placeholder('name')
                        ->attribute('maxlength', 191)
                        ->value($user->user->name)
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Email')->class('col-md-2 form-control-label')->for('email') }}
                  <div class="col-md-10">
                      {{ html()->email('Email')
                          ->class('form-control')
                          ->placeholder('email')
                          ->attribute('maxlength', 191)
                          ->value($user->user->email)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Phone')->class('col-md-2 form-control-label')->for('phone') }}

                  <div class="col-md-10">
                      {{ html()->text('Phone')
                          ->class('form-control')
                          ->placeholder('phone')
                          ->attribute('maxlength', 191)
                          ->value($user->user->phone)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Address')->class('col-md-2 form-control-label')->for('address') }}

                  <div class="col-md-10">
                      {{ html()->text('Address')
                          ->class('form-control')
                          ->placeholder('address')
                          ->attribute('maxlength', 191)
                          ->value($user->user->address)
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="row">

            </div>
            
            

    </div>
   
  </div>
  {{-- {{ html()->closeModelForm() }} --}}
</div>
@endsection

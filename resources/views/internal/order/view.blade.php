@extends('layouts.backend')

@section('content')
<div class="container-fluid">
{{-- @role('member')
    {{ html()->modelForm($user, 'PATCH', route('member.user.update'))->class('form-horizontal')->acceptsFiles()->open() }}
@else
    {{ html()->modelForm($user, 'PATCH', route('user.update'))->class('form-horizontal')->acceptsFiles()->open() }}
@endrole --}}
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">User Detail</h6>
    </div>
    <div class="card-body">
        <div class="row mt-4 mb-4">
          <div class="col-md-6">

            <div class="form-group row">
            {{ html()->label('Hotel')->class('col-md-2 form-control-label')->for('hotel') }}
                <div class="col-md-10">
                    {{ html()->text('Asigned to Hotel')
                        ->class('form-control')
                        ->value($order->hotel->name)
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Room')->class('col-md-2 form-control-label')->for('hotel') }}
                  <div class="col-md-10">
                      {{ html()->text('Position')
                          ->class('form-control')
                          ->value($order->room->type.'-'.$order->room->number)
                          ->required() }}
                  </div><!--col-->
              </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Check In')->class('col-md-2 form-control-label')->for('name') }}
                <div class="col-md-10">
                    {{ html()->text('Name')
                        ->class('form-control')
                        ->placeholder('name')
                        ->attribute('maxlength', 191)
                        ->value(\Carbon\Carbon::parse($order->book_start)->format('d F Y'))
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Check Out')->class('col-md-2 form-control-label')->for('email') }}
                  <div class="col-md-10">
                      {{ html()->text('Email')
                          ->class('form-control')
                          ->value(\Carbon\Carbon::parse($order->book_end)->format('d F Y'))
                          ->required() }}
                  </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
                {{ html()->label('Payment')->class('col-md-2 form-control-label')->for('email') }}
                    <div class="col-md-10">
                        {{ html()->text('Email')
                            ->class('form-control')
                            ->value($order->payment_status == 'none' ? 'Need Payment' : $order->payment_status)
                            ->required() }}
                    </div><!--col-->
              </div><!--form-group-->



    </div>

  </div>
  {{-- {{ html()->closeModelForm() }} --}}
</div>
@endsection

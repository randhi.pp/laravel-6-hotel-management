@extends('layouts.backend')

@section('content')
<div class="container-fluid">
@role('member')
{{ html()->form('POST', route('booking.store'))->class('form-horizontal')->open() }}
@else
{{ html()->form('POST', route('order.store'))->class('form-horizontal')->open() }}
@endrole
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Add New Booking</h6>
    </div>
    <div class="card-body">
        <div class="row mt-4 mb-4">
          <div class="col">
            @role('member')
                <input name="customer_id" value="{{ Auth::user()->id }}" hidden>
            @else
                <div class="form-group row">
                    {{ html()->label('Customer Name')->class('col-md-2 form-control-label')->for('customer_id') }}
                    <div class="col-md-10">
                        {{ html()->select('customer_id', $users->pluck('name','id'))
                            ->class('form-control')
                            ->required() }}
                    </div><!--col-->
                </div><!--form-group-->
            @endrole
            <div class="form-group row">
              {{ html()->label('Hotel')->class('col-md-2 form-control-label')->for('hotel_id') }}
                <div class="col-md-10">
                    {{ html()->select('hotel_id', $hotels->pluck('name','id'))
                        ->class('form-control hotel')
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
              {{ html()->label('Room Type')->class('col-md-2 form-control-label')->for('room') }}
                <div class="col-md-10">
                    {{ html()->select('room', $type->pluck('type','type'))
                        ->class('form-control')
                        ->required() }}
                    {{-- {{ html()->select('room_id')
                    ->class('form-control')
                    ->required() }} --}}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
                {{ html()->label('Room Number')->class('col-md-2 form-control-label')->for('room_id') }}
                  <div class="col-md-10">
                      {{ html()->select('room_id')
                          ->class('form-control')
                          ->required() }}
                      {{-- {{ html()->select('room_id')
                      ->class('form-control')
                      ->required() }} --}}
                  </div><!--col-->
              </div><!--form-group-->

            <div class="form-group row">
                {{ html()->label('Book Start')->class('col-md-2 form-control-label')->for('book_start') }}
                  <div class="col-md-10">
                      {{ html()->date('book_start')
                          ->class('form-control')
                          ->required() }}
                  </div><!--col-->
              </div><!--form-group-->
              <div class="form-group row">
                {{ html()->label('Book End')->class('col-md-2 form-control-label')->for('book_end') }}
                  <div class="col-md-10">
                      {{ html()->date('book_end')
                          ->class('form-control')
                          ->required() }}
                  </div><!--col-->
              </div><!--form-group-->
          </div>
        </div>

    </div>
    <div class="card-footer">
      <div class="row">
          <div class="col">
              @role('member')
              <a href="{{ route('booking.index') }}" class="btn btn-danger btn-sm">Cancel</a>
              @else
              <a href="{{ route('order.index') }}" class="btn btn-danger btn-sm">Cancel</a>
              @endrole
          </div><!--col-->

          <div class="col text-right">
            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Save</button>
              {{-- {{ form_submit('Update') }} --}}
          </div><!--row-->
      </div><!--row-->
    </div><!--card-footer-->
  </div>
  {{ html()->form()->close() }}
</div>
@endsection
@push('after-scripts')
<script>
$(document).ready(function() {
    $('#customer_id').select2({
        theme: 'bootstrap4',
        width: '100%',
    });
    $('#hotel_id').select2({
        theme: 'bootstrap4',
        width: '100%',
    });
    $('#room').select2({
        theme: 'bootstrap4',
        width: '100%',
    });

    $('#room').change(function () {
        if ($(this).val()) {
            console.log($('#hotel_id option:selected').val());
            console.log($('#room option:selected').val());
            var idHotel = $('#hotel_id option:selected').val();
            var roomType = $('#room option:selected').val();
            // $('#room_id').prop('disabled', false);
            $('#room_id').select2({
                width: '100%',
                theme: 'bootstrap4',
                minimumResultsForSearch: 1,
                placeholder: "Ketik 1 huruf untuk cari",
                ajax: {

                    url: '/api/room/available?hotel_id=' + idHotel +'&room=' + roomType,
                    type: 'GET',
                    dataType: 'json',
                    contentType:'application/json',
                    data: function (params) {
                        var query = {
                            q: params.term,
                        };
                        return query;
                    },
                    processResults: function (data, params) {
                        var myResults = [];
                        //console.log(data);
                        $.each(data, function (key, val) {
                            myResults.push({
                                'id': val.id,
                                'text': '['+ val.number + '] ' + val.type + ' - Rp ' + val.price
                            });
                        });
                        params.page = params.page || 1;
                        var paginate = (params.page * 10) < data.count_filtered;

                        return {
                            results: myResults,
                            pagination: {
                                more: paginate
                            }
                        };

                    },
                    //cache: true
                }
            });
        }
    });
});
</script>
@endpush

@extends('layouts.backend')

@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Order List</h1>
  <p class="mb-4">Admin / Manager / Member can list order to a room in hotel, based on each role.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Order List
        @role('member')
        <a href="{{ route('booking.create') }}" type="button" class="btn btn-sm btn-primary float-right"><i class="fa fa-sign"></i> Add Booking Order</a></h6>
        @else
      <a href="{{ route('order.create') }}" type="button" class="btn btn-sm btn-primary float-right"><i class="fa fa-sign"></i> Add Booking Order</a></h6>
        @endrole
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-sm">
          <thead class="thead">
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Cutomer Name</th>
              <th class="text-center">Hotel</th>
              <th class="text-center">Room Type & Number</th>
              <th class="text-center">Book Start</th>
              <th class="text-center">Book End</th>
              <th class="text-center">Status</th>
              <th class="text-center">Last Update</th>
              <th ></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($orders as $key => $order)
              <tr>
                <td class="text-center">ID: {{ $order->id }}</td>
                <td>{{ $order->user->name }}</td>
                <td>{{ $order->hotel->name }}</td>
                <td>{{ Ucwords($order->room->type).' ['.$order->room->number.']' }}</td>
                <td>{{ \Carbon\Carbon::parse($order->book_start)->format('d F Y') }}</td>
                <td>{{ \Carbon\Carbon::parse($order->book_end)->format('d F Y') }}</td>
                <td>{{ $order->payment_status }}</td>
                <td>{{ $order->updated_at->diffForHumans() }}</td>
                <td >
                  <div class="btn-group" role="group" aria-label="User Actions">
                    @role('member')
                    <a href="{{ route('booking.show', $order->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="View">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('booking.edit', $order->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-primary" data-original-title="Edit">
                        <i class="fas fa-edit"></i>
                    </a>
                    @else
                    <a href="{{ route('order.show', $order->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="View">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('order.edit', $order->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-primary" data-original-title="Edit">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endrole
                    <button type="button" id="btnDelete" data-id="{{$order->id}}" data-name="{{$order->user->name}}"  class="btn btn-danger btnDelete" >
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {{ $orders->links()  }}
      </div>
    </div>
  </div>

</div>
@endsection
@push('after-scripts')
<script>
$(document).ready(function() {
    $('.btnDelete').each(function(index) {
        $(this).on("click", function(){
            var id = $(this).data('id');
            var name = $(this).data('name');
            var content = document.createElement('div');
                content.innerHTML = 'Anda yakin ingin hapus Order dari Hotel? <code>'+name+'</code>';

            Swal.fire({
                // position: 'bottom-end',
                icon: 'error',
                title: 'Delete Order?',
                html: content,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                // toast: true,
                // timer: 3000,
                // showConfirmButton: false,

                // footer: '<a href>Why do I have this issue?</a>'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: "DELETE",
                        url: "order/"+id,

                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                            'Deleted!',
                            'Your Order has been deleted.',
                            'success'
                            ).then(function() {
                                window.location.reload(1);
                            });

                        },
                        error: function (response) {
                            console.log(response.responseJSON.message);
                            Swal.fire(
                            'Error!',
                            response.responseJSON.message,
                            'error'
                            );
                        }
                    })

                }
                })
            });
        });
});
</script>
@endpush

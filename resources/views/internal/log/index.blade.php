@extends('layouts.backend')

@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Activity Log</h1>
  <p class="mb-4">Admin can list all internal activity.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Log List</h6>

    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-sm table-striped table-bordered">
          <thead class="thead">
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Description</th>
              <th class="text-center">Model</th>
              <th class="text-center">Remarks</th>
              <th class="text-center">Caused By</th>
              <th class="text-center">Caused At</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($logs as $key => $log)
              <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $log->description }}</td>
                <td>{{ $log->subject_type }}</td>
                
                @php
                  $properties = json_decode ($log->properties);

                  // dd($properties);
                @endphp

              
                @if(isset($properties->old))
                <td>Old Value 
                  <pre class="text-light bg-dark m-2 p-2">{!! print_r($properties->old) !!}</pre>
                  New Value
                  <pre class="text-light bg-secondary m-2 p-2">{!! print_r($properties->attributes) !!}</pre></td>
                @elseif(isset($properties->user))
                  <td><pre class="bg-success m-2 p-2">{!! print_r($properties->user) !!}</pre></td>
                @elseif(isset($properties->staff))
                  <td><pre class="bg-success m-2 p-2">{!! print_r($properties->staff) !!}</pre></td>
                @elseif(isset($properties->order))
                  <td><pre class="bg-success m-2 p-2">{!! print_r($properties->order) !!}</pre></td>
                @else
                  <td></td>
                @endif

                <td>{{ $log->causer_id ? $log->causer->name : ''  }}</td>
                <td>{{ $log->created_at->diffForHumans() }}</td>
              </tr>
              {{-- <tr>
                <td class="text-center">{{ $key+1 }}</td>
                <td>{{ $hotel->code }}</td>
                <td>{{ $hotel->name }}</td>
                <td class="text-center">{{ $hotel->rooms()->count() }}</td>
                <td>{{ $hotel->manager->name }}</td>
                <td>{{ $hotel->address }}</td>
                <td>{{ $hotel->phone }}</td>
                <td>{{ $hotel->updated_at->diffForHumans() }}</td>
                <td >
                  <div class="btn-group" role="group" aria-label="User Actions">
                    <a href="{{ route('hotel.show', $hotel->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-info" data-original-title="View">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('hotel.edit', $hotel->id ) }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-primary" data-original-title="Edit">
                        <i class="fas fa-edit"></i>
                    </a>
                    <button type="button" id="btnDelete" data-id="{{$hotel->id}}" data-name="{{$hotel->name}}"  class="btn btn-danger btnDelete" >
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                </td>
              </tr> --}}
            @endforeach
          </tbody>
        </table>
        {{ $logs->links() }}
      </div>
    </div>
  </div>

</div>
@endsection


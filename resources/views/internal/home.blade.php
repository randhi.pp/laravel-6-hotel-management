@extends('layouts.backend')

@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
 
  
    @if (session('status'))
      <div class="alert alert-success" role="alert">
          {{ session('status') }}
      </div>
    @endif

    You are logged in as {{ Auth::user()->name }}
    @role('admin')
        <p class="mb-4">This is visible to users with the admin role. Gets translated to
        \Laratrust::hasRole('admin')</p>
    @endrole
    @role('manager')
        <p class="mb-4">This is visible to users with the manager role. Gets translated to
        \Laratrust::hasRole('manager')</p>
        <p class="mb-4">{{ Auth::user()->name }} are manager of :<br>
            
                @foreach ($hotels as $hotel)
                &bull; {{ $hotel->name }}<br>
                @endforeach
            
        </p>
        
    @endrole
    @role('staff')
        <p class="mb-4">This is visible to users with the staff role. Gets translated to
        \Laratrust::hasRole('staff')</p>
    @endrole
    @role('member')
        <p class="mb-4">This is visible to users with the member role. Gets translated to
        \Laratrust::hasRole('member')</p>
    @endrole


</div>
@endsection

@extends('layouts.app')
@push('before-scripts')
<style>
.card-img-top {
    max-height: 300px;
    min-height: 200px;
    object-fit: cover;
}
</style>
@endpush
@section('content')
<div class="jumbotron">
    <div class="container">
        <h1 class="display-4">DAFAM HOTEL GROUP</h1>
        <p class="lead">Welcome to Dafam Booking System.</p>
        <hr class="my-4">
        <p>You can book room from all Hotel in Dafam Group from here.</p>
    </div>
</div>
<div class="container">
    <div class="card-deck">
        @foreach ($hotels->take(3) as $hotel)
            <div class="card" >
            <img class="card-img-top" src="{{ Storage::disk('s3')->url($hotel->image_url) }}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">{{ $hotel->name }}</h5>
                  <p class="card-text">{{ $hotel->about }}</p>
                  <a href="/member/booking/create" class="btn btn-primary">Book Now</a>
                </div>
              </div>
        @endforeach
    </div>
</div>
@endsection


<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});


Route::group([

    'middleware' => 'api',
    'prefix' => 'hotel'

], function ($router) {

    Route::get('list', 'HotelController@apiIndex');

});

Route::group([

    'middleware' => 'api',
    'prefix' => 'room'

], function ($router) {

    Route::get('list', 'RoomController@apiIndex');
    Route::get('booked', 'RoomController@bookedSearch');
    Route::get('available', 'RoomController@getRoom');
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'user'

], function ($router) {

    Route::post('create', 'APIUserController@store');
    Route::patch('update', 'APIUserController@update');
    Route::get('show', 'APIUserController@show');
    Route::delete('delete/{id}', 'APIUserController@destroy');
});

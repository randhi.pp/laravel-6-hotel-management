<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/', 'GuestController@index')->name('guest');

Auth::routes(['verify' => true]);

Route::group(['prefix' => 'internal', 'middleware' => ['role:admin|manager|staff']], function() {
    Route::get('/home', 'InternalController@index')->name('home');
    Route::resource('order', 'OrderController');
});

Route::group(['prefix' => 'internal', 'middleware' => ['role:admin|manager']], function() {
    Route::resource('hotel', 'HotelController');
    Route::resource('log', 'ActivityLogController');
    Route::resource('staff', 'StaffController');

});

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {
    Route::get('user', 'UserController@userIndex')->name('user.index');
    Route::get('user/{id}/view', 'UserController@userView')->name('user.view');
    Route::get('user/{id}/edit', 'UserController@userEdit')->name('user.edit');
    Route::patch('user/update', 'UserController@userUpdate')->name('user.update');
    Route::get('user/create', 'UserController@userCreate')->name('user.create');
    Route::post('user/store', 'UserController@store')->name('user.store');
    Route::delete('user/{id}/delete', 'UserController@destroy')->name('user.destroy');

});

Route::group(['prefix' => 'member', 'middleware' => ['verified','role:member']], function() {
    Route::get('/home', 'MemberController@index')->name('member.home');
    Route::patch('user/update', 'UserController@userUpdate')->name('member.user.update');
    Route::resource('booking', 'OrderController');
    // Route::get('/manage', ['middleware' => ['permission:manage-admins'], 'uses' => 'AdminController@manageAdmins']);
});



Route::get('/profile/edit', 'UserController@userEdit')->name('profile.edit');
Route::get('/profile/view', 'UserController@userView')->name('profile.view');

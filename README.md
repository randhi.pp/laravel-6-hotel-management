# Hotel Management with Laravel 6

### Update : 

#### Example app in heroku

http://dafam-test.herokuapp.com

using S3 AWS storage filesystem

#### API Docs

api postman : https://documenter.getpostman.com/view/6587471/SWTHavCW?version=latest ( imported from insomnia, ugly )
api insomnia : http://dafam-test.herokuapp.com/apidocs/


#### Penggunaan
- Clone repo ini <code>git clone https://gitlab.com/randhi.pp/laravel-6-hotel-management.git</code> 
jika develop di windows menggunakan laragon di folder `c:\laragon\www\hotel`

- Jalankan `composer install`
- Jalankan `yarn install && yarn run dev`

- Sesuaikan database host, db_name, username, dan password di .env ( atau copy env.example ke .env)

- Create Key <code>php artisan key:generate</code>
- Create JWT key <code>php artisan jwt:secret</code>

- Migrate Database <code>php artisan migrate --seed</code>
- Jalankan `php artisan storage:link`

- Reload apache di laragon

Aplikasi bisa dibuka di 'http://hotel.test'



##### Jika Error
- Reload apache di laragon
- Jalankan `composer dump-autoload`
- Jalankan `php artisan optimize`
- Jalankan `php artisan route:clear`


#### User & Password

##### Admin
user: `admin@app.com` 
password: `password`

##### Manager 
###### ( + additional random 20 manager )
user: `manager@app.com` 
password: `password`

##### Staff 
###### ( + additional random 20 staff )
user: `staff@app.com` 
password: `password`

##### Member 
###### ( + additional random 20 member )
user: `member@app.com` 
password: `password`


#### Work in Progress Feature

1. Create a Laravel Application using Laravel 6.0 :heavy_check_mark: 
2. Have 4 Role: Admin, Manager, and Staff, Member using Laratrust :heavy_check_mark: 

3. Have dashboard to Manage User and Roles (you could use Admin LTE) :white_check_mark: 
- User Management Done
- Role Management WIP

4. Have API Documentation to Create, Read, Update, and Delete Users and Role (using postman docs publish) :white_check_mark: 
- API Docs URL => http://hotel.test/apidocs

5. Using JWT to protect the API :heavy_check_mark: 

6. Implement Audit Log (log all action: login, register, crud, etc) :white_check_mark: 
7. Users could Login with Google
8. Implement https://www.inspector.dev (it's free) and Laravel Telescope :heavy_check_mark: 
9. Upload it to gitlab public :heavy_check_mark: 
10. Auto Deploy to Heroku using CI/CD from Gitlab

#### Admin Could:
1. Create, Read, Edit, and Delete Users :heavy_check_mark: 
2. Create, Read, Edit, and Delete Hotels :heavy_check_mark: 
3. Create, Read, Edit, and Delete Rooms, hotel and room have one-to-many relation 
4. Assign Manager to a Hotel, a manager could manage several hotels :heavy_check_mark: 
5. Assign Staff to a Hotel, a staff could only assigned to a hotel, and only Manager/Admin who could assign the staff to a Hotel
6. Manage Hotel Orders

#### Managers Could: (only the hotels where he is assigned)
1. Read, Edit Hotels :heavy_check_mark: 
2. Read, Edit Delete Rooms, hotel and room have one-to-many relation :heavy_check_mark: 
3. Assign Staff to a Hotel, a staff could only assigned to a hotel, and only Manager/Admin who could assign the staff to a Hotel
4. Manage Hotel Orders (Read, Update Status)

#### Staff Could: (only the hotels where he is assigned)
1. Manage Hotel Orders (Read, Update Status)


## Screenshoot:
![](screenshoot/landing.png)
*Landing Page*

![](screenshoot/login.png)
*Login Page*

![](screenshoot/backend/backend1.png)
*Backend*

![](screenshoot/apidocs.png)
*API Documentation*

![](screenshoot/apiNotification.png)
*API Notif Email from create user*

![](screenshoot/telescope.png)
*Telescope Enabled*

![](screenshoot/backend/backend2.png)
*Backend*

![](screenshoot/backend/backend3.png)
*Backend*

![](screenshoot/backend/backend4.png)
*Backend*

![](screenshoot/backend/backend5.png)
*Backend*

![](screenshoot/backend/backend6.png)
*Backend*

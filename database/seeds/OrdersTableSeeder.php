<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        
        $hotels = \App\Hotel::all();

        foreach($hotels as $hotel) {

            \App\Order::create([
                'hotel_id' => $hotel->id,
                'room_id' => $hotel->id,
                'customer_id' => 64,
                'payment_status' => 'paid',
                'book_start' => \Carbon\Carbon::now(),
                'book_end' => \Carbon\Carbon::now()->addDays(2),
            ]);
        }
        factory(App\Order::class, 100)->create();

        Schema::enableForeignKeyConstraints();
    }
}

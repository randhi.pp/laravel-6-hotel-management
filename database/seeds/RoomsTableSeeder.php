<?php

use Illuminate\Database\Seeder;
use App\Hotel;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        
        $hotels = Hotel::all();

        foreach($hotels as $hotel) {

            \App\Room::create([
                'hotel_id' => $hotel->id,
                'number' => 101,
                'type' => 'standar',
                'price' => 550000,
                'created_by' => 1,
            ]);
        }
        factory(App\Room::class, 300)->create();

        Schema::enableForeignKeyConstraints();
    }
}

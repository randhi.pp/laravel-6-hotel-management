<?php

use Illuminate\Database\Seeder;

class StaffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staff = \App\Staff::create([
            'user_id' => 33,
            'hotel_id' => 1,
            'staff_position' => 'Supervisor',
        ]);
        factory(App\Staff::class, 30)->create();
    }
}

<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $hotel = \App\Hotel::create([
            'manager_id' => 2,
            'code' => $faker->unique()->numberBetween($min = 100, $max = 500),
            'name' => 'Hotel Dafam '.$faker->city,
            'address' => $faker->streetAddress,
            'phone' => $faker->tollFreePhoneNumber,
            'about' => $faker->text,
            'image_url' => 'images/'.$faker->randomElement($array = array('1.jpg','2.jpg','3.jpg')),
            'created_by' => 1,
        ]);
        factory(App\Hotel::class, 9)->create();
    }
}

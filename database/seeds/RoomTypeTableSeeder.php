<?php

use Illuminate\Database\Seeder;

class RoomTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms = array('standar','double','deluxe','executive','studio','suite');

        foreach($rooms as $room) {
            $staff = \App\RoomType::create([
                'type' => $room
            ]);
        }
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Hotel;
use App\Staff;
use Faker\Generator as Faker;

$factory->define(Staff::class, function (Faker $faker) {
    
    $staff = User::whereRoleIs('staff')->select('id')->get()->toArray();

    foreach ($staff as $value) {
        $staffID[] = $value['id'];
    }

    $hotel = Hotel::select('id')->get()->toArray();

    foreach ($hotel as $value) {
        $hotelID[] = $value['id'];
    }

    return [
        'user_id' => $faker->unique()->randomElement($staffID),
        'hotel_id' => $faker->randomElement($hotelID),
        'staff_position' => $faker->jobTitle,
    ];
});

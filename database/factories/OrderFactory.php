<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use App\Hotel;
use App\Room;
use App\User;

use Carbon\Carbon;

use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    
    $hotel = Hotel::select('id')->get()->toArray();

        foreach ($hotel as $value) {
            $hotelID[] = $value['id'];
        }

        $selectedHotel = $faker->randomElement($hotelID);

    $roomFaker = Room::select('id')->where('hotel_id',$selectedHotel)->get()->toArray();
    
        foreach ($roomFaker as $value) {
            $roomID[] = $value['id'];
        }

    $cust = User::whereRoleIs('member')->get()->toArray();

        foreach ($cust as $value) {
            $custID[] = $value['id'];
        }
    
    $dateStart = $faker->dateTimeThisMonth;
    $dateEnd = Carbon::parse($dateStart)->addDays(2);

    return [
        'hotel_id' => $selectedHotel,
        'room_id' => $faker->randomElement($roomID),
        'customer_id' => $faker->randomElement($custID),
        'payment_status' => $faker->randomElement($array = array('paid','down payment','none')),
        'book_start' => $dateStart,
        'book_end' => $dateEnd,
    ];
});

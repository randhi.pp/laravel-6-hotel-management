<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Hotel;
use Faker\Generator as Faker;

$factory->define(Hotel::class, function (Faker $faker) {
    
    $manager = User::whereRoleIs('manager')->select('id')->get()->toArray();

    foreach ($manager as $value) {
        $managerID[] = $value['id'];
    }

    // dd($managerID);

    return [
        'manager_id' => $faker->randomElement($managerID),
        'code' => $faker->unique()->numberBetween($min = 100, $max = 500),
        'name' => 'Hotel Dafam '.$faker->city,
        'address' => $faker->streetAddress,
        'phone' => $faker->tollFreePhoneNumber,
        'about' => $faker->text,
        'image_url' => 'images/'.$faker->randomElement($array = array('1.jpg','2.jpg','3.jpg')),
        'created_by' => 1,
    ];
});

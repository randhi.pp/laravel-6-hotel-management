<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Room;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    return [
        'hotel_id' => $faker->randomDigitNotNull,
        'number' => $faker->numberBetween($min = 100, $max = 500),
        'type' => $faker->randomElement($array = array ('standar','double','deluxe','studio','suite','executive')),
        'price' => $faker->numberBetween($min = 250000, $max = 850000),
        'created_by' => 1,        
    ];
});

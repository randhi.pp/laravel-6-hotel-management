<?php

return [
    'role_structure' => [
        'admin' => [
            'users' => 'c,r,u,d',
            'acl' => 'c,r,u,d',
            'profile' => 'r,u',
            'hotels' => 'c,r,u,d',
            'rooms' => 'c,r,u,d',
            'staff' => 'c,r,u,d',
            'orders' => 'c,r,u,d',
            'managers' => 'c,r,u,d'
        ],
        'manager' => [
            'profile' => 'r,u',
            'hotels' => 'r,u',
            'rooms' => 'r,u,d',
            'staff' => 'c,r,u,d',
            'orders' => 'c,r,u,d',
            'managers' => 'r'
        ],
        'staff' => [
            'hotels' => 'r',
            'rooms' => 'r',
            'staff' => 'r',
            'profile' => 'r,u',
            'orders' => 'r,u'
        ],
        'member' => [
            'hotels' => 'r',
            'rooms' => 'r',
            'profile' => 'r,u',
            'orders' => 'c,r,u'
        ],
    ],
    'permission_structure' => [
        'cru_user' => [
            'profile' => 'c,r,u'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
